#!/bin/bash

filename=""
out_dir="./pdfs/"
filename="simulation"
sim_time=100

kernels=3

mkdir -p ${out_dir}

echo "running $filename"
NS_LOG=Simulation:nfd.KernelStore:nfd.KernelInfo:Simulation:ndn.Consumer ./waf --run="ndn-function --deadline=300 --size=100 --frequency=10 --kernels=${kernels} --time=${sim_time}"  &> ${filename}.log




echo "processing log file ${filename}.log"
./print_score.sh ${filename}.log ${kernels}


echo "moving file to ${out_dir}/${filename}.pdf"
mv output.pdf ${out_dir}/${filename}.pdf

