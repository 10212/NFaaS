#ifndef NFD_DAEMON_FW_FUNCTION_STRATEGY_HPP
#define NFD_DAEMON_FW_FUNCTION_STRATEGY_HPP

#include "ns3/random-variable-stream.h"

#include "ns3/ndnSIM/model/ndn-common.hpp"

#include "ns3/ndnSIM/NFD/daemon/face/generic-link-service.hpp"


#include "strategy.hpp"
#include "table/ks.hpp"
#include <face.hpp>
namespace nfd {
namespace fw {


class FunctionStrategy : public Strategy
{
public:
  explicit
  FunctionStrategy(Forwarder& forwarder, const Name& name = STRATEGY_NAME);

  virtual void
  afterReceiveInterest(const Face& inFace, const Interest& interest,
                       const shared_ptr<pit::Entry>& pitEntry) override;

  virtual void 
  beforeSatisfyInterest(const shared_ptr<pit::Entry>& pitEntry, const Face& inFace,
			                                          const Data& data);
               
  virtual void
  beforeExpirePendingInterest (const shared_ptr< pit::Entry > &  pitEntry);

  virtual void
  beforeUnsolicited(const Face& inFace, const Data& data);

  void 
  createFace();

  void
  getKernels();

  void onData(Data& data);
  void afterInterest(Interest& interest);


public:
  static const Name STRATEGY_NAME;
  static const time::nanoseconds MEASUREMENTS_LIFETIME;
  nfd::face::GenericLinkService* m_genericLinkService;
  shared_ptr<ndn::Face> m_face;
  


private:
  void
  sendDiscoverPackets(const Interest& oldInterest, Face& face);

  struct Container{
	  Name name;
	  ns3::Time time;
	  FaceId faceId;
  };
  std::list<struct Container> m_delays;
  unsigned int m_sentPackets = 0;
  void clean(struct Container stats);

  ndn::KeyChain m_keyChain;
};



} // namespace fw
} // namespace nfd

#endif // NFD_DAEMON_FW_FUNCTION_STRATEGY_HPP
