/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2011-2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

// ndn-simple.cpp
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/topology-read-module.h"
#include "model/ndn-l3-protocol.hpp"
#include "model/ndn-l3-protocol.hpp"
#include "helper/ndn-fib-helper.hpp"
#include "model/ndn-net-device-transport.hpp"
#include "model/ndn-global-router.hpp"

#define FREQUENCY "1"
#define KERNELS 1
#define TIME 2
#undef CLOUD 2//15
namespace ns3 {

NS_LOG_COMPONENT_DEFINE("Simulation");


int main(int argc, char* argv[]) {
	// setting default parameters for PointToPoint links and channels
	Config::SetDefault("ns3::PointToPointNetDevice::DataRate",
			StringValue("500Mbps"));
	Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
	Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("50"));


	double time = TIME;
	int kernels = KERNELS;
	string frequency = FREQUENCY;
	string size = "100";
	string deadline = "150";
	CommandLine cmd;

	cmd.AddValue("time", "Time of the simulation.", time);
	cmd.AddValue("kernels", "Number of kernels.", kernels);
	cmd.AddValue("frequency", "Number of kernels.", frequency);
	cmd.AddValue("size", "Task size", size);
	cmd.AddValue("deadline", "Task deadline", deadline);
	cmd.Parse(argc, argv);


	NodeContainer nodes;
	// Creating nodes
	nodes.Create(1);


	// Install NDN stack on all nodes
	ndn::StackHelper ndnHelper;
	ndnHelper.SetDefaultRoutes(true);
	ndnHelper.InstallAll();

	// Choosing forwarding strategy
	ndn::StrategyChoiceHelper::InstallAll("/",
			"/localhost/nfd/strategy/best-route2");
	ndn::StrategyChoiceHelper::InstallAll("/exec",
			"/localhost/nfd/strategy/function");
	// Installing global routing interface on all nodes

	// Install NDN applications
	std::string prefix_cloud = "/cloud/";
	std::string prefix_exec = "/exec/";
	// Installing applications

	// Consumer
	ndn::AppHelper consumerHelper("ns3::ndn::ConsumerTask");
	// Consumer will request /prefix/0, /prefix/1, ...
	//
	consumerHelper.SetAttribute("Frequency", StringValue(frequency)); // 1 interests a second
	consumerHelper.SetAttribute("TaskSize", StringValue(size)); // 1 interests a second
	consumerHelper.SetAttribute("TaskDeadline", StringValue(deadline)); // 1 interests a second
	consumerHelper.SetAttribute("StartTime", StringValue("0s"));
	//consumerHelper.SetAttribute("StopTime", StringValue("1s"));
	consumerHelper.SetAttribute("Randomize", StringValue("uniform"));

	for (int i = 1; i <= kernels; i++) {
		std::stringstream ss;
		ss << "/exec/example/" << i;
		consumerHelper.SetPrefix(ss.str());
		std::stringstream delays;
		int delay = 10 * (i);
		delays << delay << "s";
		consumerHelper.SetAttribute("StartTime", StringValue(delays.str()));
		std::stringstream freqs;
		freqs << (i);

		consumerHelper.SetAttribute("Frequency", StringValue(freqs.str())); // 1 interests a second

		consumerHelper.Install(nodes.Get(0));

	}


	// Add /prefix origins to ndn::GlobalRouter
	ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
	ndnGlobalRoutingHelper.InstallAll();
	for (ns3::Ptr<ns3::Node> node : nodes) {
		ndnGlobalRoutingHelper.AddOrigins(prefix_exec, node);
	}
#ifdef CLOUD
	ndnGlobalRoutingHelper.AddOrigins(prefix_cloud, nodes.Get(CLOUD));
#endif
	// Calculate and install FIBs
	ndn::GlobalRoutingHelper::CalculateRoutes();

	Simulator::Stop(Seconds(time));

	Simulator::Run();
	Simulator::Destroy();

	return 0;
}

} // namespace ns3

int main(int argc, char* argv[]) {
	return ns3::main(argc, argv);
}
