#!/usr/bin/gnuplot
NAME="output"
load 'style.gp'
set key top left
set title 'm=2, n=5'
set xlabel "Time[s]"
set ylabel "Score"
set output NAME.".pdf"
plot \
NAME.".dat" using 1:2 title '1' with linespoints ls 1, \
NAME.".dat" using 1:3 title '2' with linespoints ls 2, \
